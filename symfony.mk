PHP_SERVICE := php

# Command for composer install
symfony-composer-install:
	@docker-compose exec -T $(PHP_SERVICE) composer install

# Command for create database
#
# Use `make symfony-database-create` for create database
# Use `make symfony-database-create postgis=true` for create database with postgis extension
symfony-database-create:
	@docker-compose exec -T $(PHP_SERVICE) bin/console doctrine:database:create --if-not-exists
	@docker-compose exec -T $(PHP_SERVICE) bin/console doctrine:database:create --if-not-exists --env=test
ifeq	($(postgis),true)
				@docker-compose exec -T $(PHP_SERVICE) bin/console d:q:s 'create extension postgis;'
				@docker-compose exec -T $(PHP_SERVICE) bin/console d:q:s 'create extension postgis;' --env=test
endif

# Execution default command by bin/console
#
# Use `make symfony-console c='command'`
symfony-console:
	@docker-compose exec -T $(PHP_SERVICE) bin/console ${c}

symfony-schema-update:
	@docker-compose exec -T $(PHP_SERVICE) bin/console doctrine:schema:update --force
	@docker-compose exec -T $(PHP_SERVICE) bin/console doctrine:schema:update --force --env test

symfony-fixtures-load:
	@docker-compose exec -T $(PHP_SERVICE) bin/console doctrine:fixtures:load --no-interaction

symfony-performance:
	@docker-compose exec -T $(PHP_SERVICE) touch file.txt && time dd if=/dev/zero of=file.txt bs=1024 count=100000 && rm file.txt

php-bash:
	@docker-compose exec $(PHP_SERVICE) bash
