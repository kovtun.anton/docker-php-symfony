#!/bin/bash
source ./.env

# Exported variables from .env
export APP_CONTAINER_PATH=${APP_CONTAINER_PATH}
export PROJECT_HOST_NAME=${PROJECT_HOST_NAME}

eval "git clone ${PROJECT_GIT_REPO} ${APP_LOCAL_PATH}"

cp ./symfony/.env.dist ./symfony/.env
cp ./symfony/.env.test.dist ./symfony/.env.test

CONFIG_FILE='./docker/conf/nginx/symfony.conf'
DIST_CONFIG_FILE='./docker/conf/nginx/symfony.conf.dist'
envsubst "$(env | sed -e 's/=.*//' -e 's/^/\$/g')" < ${DIST_CONFIG_FILE} > ${CONFIG_FILE}

make -i app-build
