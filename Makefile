#!/usr/bin/make
include ./*.mk
include .env

app-build:
				make docker-build
				make docker-up
				make symfony-composer-install
				make symfony-database-create
				make symfony-schema-update
				make symfony-fixtures-load
				make symfony-performance

app-up:
				make docker-up
				make symfony-composer-install
				make symfony-schema-update
				make symfony-performance
